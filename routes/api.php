<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
*/

Route::prefix('v1')
    ->name('api.v1.')
    ->middleware('api')
    ->group(function () {
        Route::post('/games', 'Api\v1\GameController@store');
    });
